package org.acme.application.controller.tag;

import java.util.List;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;

import org.acme.entity.common.Paginated;
import org.acme.entity.common.PaginationRequest;
import org.acme.entity.post.PostEntity;
import org.acme.entity.post.PostRequest;
import org.acme.entity.tag.TagEntity;
import org.acme.infrastructure.database.repository.post.PostRepository;
import org.acme.infrastructure.database.repository.provider.BaseRepository;
import org.acme.infrastructure.database.repository.tag.TagRepository;

@QuarkusTest
public class TagResourceTest {
    @Inject
    TagRepository tagRepository;
    @Test
    public void testGetEndpointShouldSuccessWhenIdIsExists() {
         TagRepository mock = Mockito.mock(TagRepository.class);  
           
       
        TagEntity t = new TagEntity();
        t.setId((long)1);
        t.setLabel("some title");

        List<TagEntity> list = new ArrayList<>();
        list.add(t);
        Mockito.when(mock.listAll()).thenReturn(list);
        QuarkusMock.installMockForType(mock, TagRepository.class);
        
        given()
          .when()
          .header("Content-Type", "application/json")
          .get("/tags")
          .then()
             .statusCode(Response.Status.OK.getStatusCode())
             .body("[0].label", is("some title"));
    }
}