package org.acme.application.controller.post;

import java.util.List;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;

import org.acme.entity.common.Paginated;
import org.acme.entity.common.PaginationRequest;
import org.acme.entity.post.PostEntity;
import org.acme.entity.post.PostRequest;
import org.acme.infrastructure.database.repository.post.PostRepository;
import org.acme.infrastructure.database.repository.provider.BaseRepository;

@QuarkusTest
public class PostResourceTest {
    @Inject
    PostRepository postRepository;
    @Test
    public void testCreateEndpointShouldSuccessWhenRequestIsValid() {

        JsonObject requestParams = new JsonObject()
            .put("title", "some title")
            .put("content", "some content")
            .put("tags", 
                new JsonArray()
                .add("tag1")
                .add("tag2"));
                
        given()
          .when()
          .header("Content-Type", "application/json")
          .body(requestParams.encode())
          .post("/posts")
          .then()
             .statusCode(Response.Status.CREATED.getStatusCode())
             .body("title", is("some title"))
             .body("content", is("some content"));
    }

    @Test
    @Transactional
    public void testUpdateEndpointShouldSuccessWhenRequestIsValid() {
         PostRepository mock = Mockito.mock(PostRepository.class);  
           
        PostEntity p = new PostEntity();
        p.setId((long)1);
        p.setTitle("some title");
        p.setContent("some content");
        Mockito.when(mock.findById((long)1)).thenReturn(p);
        QuarkusMock.installMockForType(mock, PostRepository.class);
        
        JsonObject requestParams = new JsonObject()
            .put("title", "another title")
            .put("content", "another content")
            .put("tags", 
                new JsonArray()
                .add("tag1")
                .add("tag2"));
                
        given()
          .when()
          .header("Content-Type", "application/json")
          .body(requestParams.encode())
          .pathParam("id", 1)
          .put("/posts/{id}",1)
          .then()
             .statusCode(Response.Status.ACCEPTED.getStatusCode())
             .body("title", is("another title"))
             .body("content", is("another content"));
    }
    public void testUpdateEndpointShouldThrow404WhenIdDoesNotExists() {
         PostRepository mock = Mockito.mock(PostRepository.class);  
           
       
        Mockito.when(mock.findById((long)1)).thenReturn(null);
        QuarkusMock.installMockForType(mock, PostRepository.class);
        
        JsonObject requestParams = new JsonObject()
            .put("title", "another title")
            .put("content", "another content")
            .put("tags", 
                new JsonArray()
                .add("tag1")
                .add("tag2"));
                
        given()
          .when()
          .header("Content-Type", "application/json")
          .body(requestParams.encode())
          .pathParam("id", 1)
          .put("/posts/{id}",1)
          .then()
             .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }
    public void testGetEndpointShouldThrow404WhenIdDoesNotExists() {
         PostRepository mock = Mockito.mock(PostRepository.class);  
           
       
        Mockito.when(mock.findById((long)1)).thenReturn(null);
        QuarkusMock.installMockForType(mock, PostRepository.class);
        
        given()
          .when()
          .header("Content-Type", "application/json")
          .pathParam("id", 1)
          .get("/posts/{id}",1)
          .then()
             .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }
    public void testGetEndpointShouldSuccessWhenIdIsExists() {
         PostRepository mock = Mockito.mock(PostRepository.class);  
           
       
        PostEntity p = new PostEntity();
        p.setId((long)1);
        p.setTitle("some title");
        p.setContent("some content");
        Mockito.when(mock.findById((long)1)).thenReturn(p);
        QuarkusMock.installMockForType(mock, PostRepository.class);
        
        given()
          .when()
          .header("Content-Type", "application/json")
          .pathParam("id", 1)
          .get("/posts/{id}",1)
          .then()
             .statusCode(Response.Status.OK.getStatusCode())
             .body("title", is("some title"))
             .body("content", is("some content"));
    }
     public void testDeleteEndpointShouldThrow404WhenIdDoesNotExists() {
        PostRepository mock = Mockito.mock(PostRepository.class);  
        Mockito.when(mock.findById((long)1)).thenReturn(null);
        QuarkusMock.installMockForType(mock, PostRepository.class);
        
        given()
          .when()
          .header("Content-Type", "application/json")
          .pathParam("id", 1)
          .delete("/posts/{id}",1)
          .then()
             .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }
    public void testDeleteEndpointShouldSuccessWhenIdIsExists() {
        PostRepository mock = Mockito.mock(PostRepository.class);  
        PostEntity p = new PostEntity();
        p.setId((long)1);
        p.setTitle("some title");
        p.setContent("some content");
        Mockito.when(mock.findById((long)1)).thenReturn(p);
        QuarkusMock.installMockForType(mock, PostRepository.class);
        
        given()
          .when()
          .header("Content-Type", "application/json")
          .pathParam("id", 1)
          .delete("/posts/{id}",1)
          .then()
             .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }
    public void testGetEndpointShouldSuccessWhenRequetsIsValid() {
        BaseRepository mock = Mockito.mock(BaseRepository.class);  
        PostEntity p = new PostEntity();
        p.setId((long)1);
        p.setTitle("some title");
        p.setContent("some content");
        PaginationRequest request = new PaginationRequest();
        PanacheQuery q = postRepository.find("some-field", "some-value");
        Paginated<PostEntity> paginated = new Paginated<>();
        paginated.setTotal((long)1);
        paginated.setPage(1);
        List<PostEntity> mockData = new ArrayList<PostEntity>();
        paginated.setData(mockData);
        Mockito.when(mock.paginate(q, request)).thenReturn(paginated);
        QuarkusMock.installMockForType(mock, BaseRepository.class);
        
        given()
          .when()
          .header("Content-Type", "application/json")
          .get("/post")
          .then()
             .statusCode(Response.Status.OK.getStatusCode())
             .body("total", is(1))
             .body("data[0].title", is("some title"));
    }
}