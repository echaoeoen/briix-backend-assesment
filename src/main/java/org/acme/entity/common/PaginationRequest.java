package org.acme.entity.common;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.QueryParam;

public class PaginationRequest {

    @NotBlank(message="Page may not be blank")
    @Positive(message="Page must be positive")
    @QueryParam("page")
    @DefaultValue("1")
    private int page;
    public void setPage(int page){
        this.page = page;
    }
    public int getPage(){
        return this.page;
    }

    @QueryParam("size")
    @DefaultValue("10")
    private int size;
    public void setSize(int size){
        this.size = size;
    }
    public int getSize(){
        return this.size;
    }
}
