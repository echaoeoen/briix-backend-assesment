package org.acme.entity.common;

import java.util.List;

public class Paginated<T> {
    private List<T> data;
    public List<T> getData() {
        return data;
    }
    public void setData(List<T> data) {
        this.data = data;
    }

    private Long total;
    public Long getTotal() {
        return total;
    }
    public void setTotal(Long total) {
        this.total = total;
    }

    private int page;
    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }

    private int size;
    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }
    
    private int totalPages;
    public int getTotalPages() {
        return totalPages;
    }
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
