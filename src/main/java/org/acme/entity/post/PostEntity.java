package org.acme.entity.post;

import java.util.Set;

import org.acme.entity.tag.TagEntity;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;

import jakarta.persistence.Entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.UniqueConstraint;

@Entity
(name="post_entity")
@FilterDef(name = "titleFilter")
public class PostEntity{
    private Long id;
    private String title;
    private String content;
    
    private Set<TagEntity> tags;

    @Id
    @SequenceGenerator(name = "postSeq", sequenceName = "post_id_sq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "postSeq")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Filter(name = "titleFilter", condition = "title like :title")
    public String getTitle(){
        return title;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getContent(){
        return content;
    }
    public void setContent(String content){
        this.content = content;
    }
    @Fetch(FetchMode.JOIN)
    @ManyToMany
    @JoinTable(
        name="post_tag",
        joinColumns=@JoinColumn(name="post_id", referencedColumnName="ID"),
        inverseJoinColumns=@JoinColumn(name="tag_id", referencedColumnName="ID"),
        uniqueConstraints = @UniqueConstraint(columnNames = {"post_id", "tag_id"}))
    public Set<TagEntity> getTags() {
        return tags;
    }

    public void setTags(Set<TagEntity> tags) {
        this.tags = tags;
    }
}