package org.acme.entity.post;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.acme.entity.tag.TagEntity;

import jakarta.validation.constraints.NotBlank;

public class PostRequest {
    @NotBlank(message="Title should not be blank")
    private String title;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @NotBlank(message="Content should not be blank")
    private String content;
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    private List<String> tags;
    public List<String> getTags() {
        return tags;
    }
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public PostEntity toEntity() {
        PostEntity entity = new PostEntity();
        entity.setTitle(this.title);
        entity.setContent(this.content);
        Set<TagEntity> tags = new HashSet<TagEntity>();
        for (String tag : this.tags) {
            TagEntity tagEntity = new TagEntity();
            tagEntity.setLabel(tag);
            tags.add(tagEntity);
        }
        entity.setTags(tags);
        return entity;
    }
    
}
