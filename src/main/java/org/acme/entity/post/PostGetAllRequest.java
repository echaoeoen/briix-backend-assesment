package org.acme.entity.post;


import org.acme.entity.common.PaginationRequest;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.QueryParam;

public class PostGetAllRequest extends PaginationRequest {

    @QueryParam("title")
    @DefaultValue("")
    private String title;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
