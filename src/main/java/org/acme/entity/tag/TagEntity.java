package org.acme.entity.tag;
import java.util.Set;

import org.acme.entity.post.PostEntity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity(name="tag_entity")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"label"}))
public class TagEntity {
    private Long id;
    private String label;

    @Id
    @SequenceGenerator(name = "tagSeq", sequenceName = "tag_id_sq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "tagSeq")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getLabel(){
        return label;
    }
    public void setLabel(String label){
        this.label = label;
    }

}
