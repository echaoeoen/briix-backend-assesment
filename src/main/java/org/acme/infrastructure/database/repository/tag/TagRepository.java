package org.acme.infrastructure.database.repository.tag;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.acme.entity.tag.TagEntity;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
@ApplicationScoped
public class TagRepository implements PanacheRepository<TagEntity>{
    public List<TagEntity> getAllByLabels(List<String> labels){
        return this.list("label in (?1)", labels);
    }
    public Set<TagEntity> persistAll(Set<TagEntity> tags){
        List<String> labels = new ArrayList<>();
        for (TagEntity tagEntity : tags) {
            labels.add(tagEntity.getLabel());
        }
        List<TagEntity> existedEntities = this.getAllByLabels(labels);
        for(TagEntity tag : tags){
            boolean isExist = false;
            for (TagEntity existedTag : existedEntities) {
                if(tag.getLabel().equals(existedTag.getLabel())) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                this.persistAndFlush(tag);
            }
        }
        List<TagEntity> persisted = this.getAllByLabels(labels);
        return Set.copyOf(persisted);
    }
}
