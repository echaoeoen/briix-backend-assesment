package org.acme.infrastructure.database.repository.post;

import java.util.ArrayList;
import java.util.List;

import org.acme.entity.common.Paginated;
import org.acme.entity.post.PostEntity;
import org.acme.entity.post.PostGetAllRequest;
import org.acme.infrastructure.database.repository.provider.BaseRepository;


import io.quarkus.hibernate.orm.panache.PanacheQuery;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PostRepository extends BaseRepository<PostEntity>{
    public Paginated<PostEntity> getAll(PostGetAllRequest request){
        List<PostEntity> list = list("title LIKE ?1",  '%' + request.getTitle() + '%');
        PanacheQuery<PostEntity> queryAll = 
            find("title LIKE ?1",  '%' + request.getTitle() + '%');    
        return paginate(queryAll, request);
    }
}
