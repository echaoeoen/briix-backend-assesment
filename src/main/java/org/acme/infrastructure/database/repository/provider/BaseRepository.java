package org.acme.infrastructure.database.repository.provider;

import java.util.List;

import org.acme.entity.common.Paginated;
import org.acme.entity.common.PaginationRequest;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

public class BaseRepository<T> implements PanacheRepository<T> {

    public Paginated<T> paginate(PanacheQuery<T> panacheQuery, PaginationRequest request) {
        Long total = panacheQuery.count();
        int startIndex = (request.getPage() - 1) * request.getSize();
        int endIndex = (startIndex + request.getSize() - 1); 
        PanacheQuery<T> pageQuery = panacheQuery.range(startIndex, endIndex);
        int totalPages = (int)(total % request.getSize());
        List<T> list = pageQuery.list();
        Paginated<T> paginated = new Paginated<>();
        paginated.setTotal(total);
        paginated.setPage(request.getPage());
        paginated.setSize(list.size());
        paginated.setTotalPages(totalPages);
        paginated.setData(list);
        return paginated;
    }
}
