package org.acme.application.controller.post;

import org.acme.domain.post.PostService;
import org.acme.entity.post.PostEntity;
import org.acme.entity.post.PostGetAllRequest;
import org.acme.entity.post.PostRequest;

import jakarta.inject.Inject;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/")
public class PostResource {
    @Inject
    private PostService postService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON) 
    @Path("/posts")
    public Response create(
        PostRequest request) {
        PostEntity p = this.postService.create(request);
        return Response.status(Response.Status.CREATED).entity(p).build();
    }

    @GET
    @Path("/posts/{id}")
    public Response get(
        @PathParam("id") Long id) {
        PostEntity p = this.postService.getById(id);
        return Response.status(Response.Status.OK).entity(p).build();
    }
    @PUT
    @Consumes(MediaType.APPLICATION_JSON) 
    @Path("/posts/{id}")
    public Response update(
        @PathParam("id") Long id,
        PostRequest request) {
        PostEntity p = this.postService.update(id, request);
        return Response.status(Response.Status.ACCEPTED).entity(p).build();
    }
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON) 
    @Path("/posts/{id}")
    public Response delete(
        @PathParam("id") Long id) {
        this.postService.delete(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
    @GET
    @Path("/posts")
    public Response getAll(@BeanParam PostGetAllRequest request) {
        return Response.status(Response.Status.OK).entity(this.postService.getAll(request)).build();
    }
}
