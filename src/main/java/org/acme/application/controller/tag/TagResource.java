package org.acme.application.controller.tag;

import org.acme.domain.tag.TagService;
import org.acme.entity.tag.TagEntity;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.List;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
@Path("/")
public class TagResource {

    @Inject
    TagService tagService; 

    @GET
    @Path("/tags")
    public Response getAll() {
        List<TagEntity> tags = tagService.getAll();
        return Response.status(Response.Status.OK).entity(tags).build();
    }
}
