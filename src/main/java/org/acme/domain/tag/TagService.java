package org.acme.domain.tag;

import java.util.List;

import org.acme.entity.tag.TagEntity;
import org.acme.infrastructure.database.repository.tag.TagRepository;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class TagService {
    @Inject
    TagRepository tagRepository;

    public List<TagEntity> getAll() {
        return tagRepository.listAll();
    }
}
