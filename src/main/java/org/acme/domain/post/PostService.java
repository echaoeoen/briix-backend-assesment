package org.acme.domain.post;

import org.acme.entity.common.Paginated;
import org.acme.entity.post.PostEntity;
import org.acme.entity.post.PostGetAllRequest;
import org.acme.entity.post.PostRequest;
import org.acme.infrastructure.database.repository.post.PostRepository;
import org.acme.infrastructure.database.repository.tag.TagRepository;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
@ApplicationScoped
public class PostService {

    @Inject
    PostRepository postRepository; 
    @Inject
    TagRepository tagRepository; 

    @Transactional
    public PostEntity create(PostRequest post) {
        PostEntity entity = post.toEntity();
        entity.setTags(tagRepository.persistAll(entity.getTags()));
        postRepository.persistAndFlush(entity);
        return entity;
    }
    @Transactional
    public PostEntity getById(Long id) {
        PostEntity entity = postRepository.findById(id);
        if(entity == null) {
            throw new WebApplicationException("Post not found", Response.Status.NOT_FOUND);
        }
        return entity;
    }
    @Transactional
    public PostEntity update(Long id, PostRequest post) {
        PostEntity existed = this.getById(id); 
        existed.setId(id);
        existed.setTitle(post.getTitle());
        existed.setContent(post.getContent());
        existed.setTags(tagRepository.persistAll(post.toEntity().getTags()));
        return existed;
    }
    @Transactional
    public PostEntity delete(Long id) {
        PostEntity existed = this.getById(id); 
        postRepository.delete(existed);
        return existed;
    }

    @Transactional
    public Paginated<PostEntity> getAll(PostGetAllRequest request){
        Paginated<PostEntity> response = postRepository.getAll(request);
        return response;
    }
    
}
